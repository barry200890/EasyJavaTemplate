import request from '@/utils/request'

/**
* 查询数据（分页）
*/
export function get${entity}TableData(params) {
  return request({
    url: '/${table.entityPath}/pagelist',
    method: 'post',
    data: params
  })
}

/**
* 查询数据（不分页）
*/
export function get${entity}List() {
  return request({
    url: '/${table.entityPath}/list',
    method: 'get',
  })
}

/**
* 批量删除
*/
export function delete${entity}(ids) {
  return request({
    url: '/${table.entityPath}/batchDelete',
    method: 'post',
    data: ids,
  })
};


/**
* 新增数据
*/
export function add${entity}(params) {
  return request({
    url: '/${table.entityPath}/save',
    method: 'post',
    data: params
  })
};

/**
* 修改数据
*/
export function update${entity}(params) {
  return request({
    url: '/${table.entityPath}/update',
    method: 'post',
    data: params
  })
};