DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
                             `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
                             `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
                             `real_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
                             `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
                             `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
                             `avatar` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像地址',
                             `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
                             `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
                             `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
                             `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
                             `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
                             `update_user_id` bigint(20) NULL DEFAULT NULL COMMENT '更新人',
                             PRIMARY KEY (`id`) USING BTREE,
                             UNIQUE INDEX `un_user_name`(`user_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (109, 'admin', '波波老师', '1134135987@qq.com', '18086290000', 'https://img0.baidu.com/it/u=3361482875,3939563024&fm=253&fmt=auto&app=138&f=JPEG', 'e10adc3949ba59abbe56e057f20f883e', '管理员账号', '2023-05-01 18:47:50', 109, '2023-05-14 18:13:04', 109);
INSERT INTO `sys_user` VALUES (110, 'test', '王天霸', '1134135987@qq.com', '18088888888', 'https://img0.baidu.com/it/u=3361482875,3939563024&fm=253&fmt=auto&app=138&f=JPEG', 'e10adc3949ba59abbe56e057f20f883e', '123456', '2023-05-01 18:47:50', 109, '2023-05-14 18:13:09', 109);

SET FOREIGN_KEY_CHECKS = 1;
