import request from '@/utils/request'

/**
 * 删除用户
 * @param data
 * @returns {AxiosPromise}
 */
export function deleteUser(data) {
  return request({
    url: '/user/batchDelete',
    method: 'post',
    data
  })
}

/**
 * 编辑用户
 * @param data
 * @returns {AxiosPromise}
 */
export function updateUser(data) {
  return request({
    url: '/user/update',
    method: 'post',
    data
  })
}

/**
 * 新增用户
 * @param data
 * @returns {AxiosPromise}
 */
export function addUser(data) {
  return request({
    url: '/user/save',
    method: 'post',
    data
  })
}

/**
 * 获取用户信息列表
 * @param data
 * @returns {AxiosPromise}
 */
export function getUserList(data) {
  return request({
    url: '/user/pagelist',
    method: 'post',
    data
  })
}

/**
 * 登录
 * @param data
 * @returns {AxiosPromise}
 */
export function login(data) {
  return request({
    url: '/login/login',
    method: 'post',
    data
  })
}

/**
 * 获取登录者信息
 * @param token
 * @returns {AxiosPromise}
 */
export function getInfo(token) {
  return request({
    url: '/user/info',
    method: 'get',
    params: { token }
  })
}

/**
 * 注销
 * @param token
 * @returns {AxiosPromise}
 */
export function logout(token) {
  return request({
    url: '/login/logout',
    method: 'get',
    params: { token }
  })
}
