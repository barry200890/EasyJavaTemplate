package cn.wujiangbo.constants;

/**
 * @description 系统常量类
 * @author 波波老师(weixin:javabobo0513)
 */
public class SystemConstants {

    //登录成功之后token超时时间（单位：分钟）
    public final static int LOGIN_TIME_OUT = 60;

    //新用户注册的默认登录密码
    public final static String DEFAULT_USER_LOGIN_PWD = "123456";

    //新用户注册的默认头像
    public final static String DEFAULT_USER_AVATAR = "https://easyjava.oss-cn-chengdu.aliyuncs.com/common/0161a7079e574da287dc182bdf756abc.jpg";
}
