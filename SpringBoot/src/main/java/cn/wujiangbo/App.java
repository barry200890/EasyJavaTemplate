package cn.wujiangbo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @desc：系统启动类
 * @author 波波老师(weixin:javabobo0513)
 */
@SpringBootApplication
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
        System.out.println("**************************************");
        System.out.println("**************系统启动成功*************");
        System.out.println("**************************************");
    }
}